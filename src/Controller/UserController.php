<?php

namespace App\Controller;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{

    /**
     * @Route("/api/v1/client", name="Add Client", methods={"POST", "HEAD"}, schemes={"HTTP"}, host="127.0.0.1")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();

        $data = $request->request->all();

        $user = new User();
        $user->setLogin($data['username']);
        $user->setPassword($encoder->encodePassword($user, $data['password']));
        $em->persist($user);
        $em->flush();

        return new JsonResponse(['User Registered']);
    }

    /**
     * @Route("/login", name="Login Client", methods={"POST", "HEAD"}, schemes={"HTTP"}, host="127.0.0.1")
     */
    public function login(Request $request, UserPasswordEncoderInterface $encoder, JWTTokenManagerInterface $JWTManager)
    {
        $em = $this->getDoctrine()->getManager();

        $login = $request->request->get('username');
        $password = $request->request->get('password');

        $user = $em->getRepository(User::class)->findOneBy(['login' => $login]);

        $isPasswordValid = $encoder->isPasswordValid($user, $password);

        if ($isPasswordValid) {
            return new JsonResponse(['token' => $JWTManager->create($user)]);
        }

        return new JsonResponse('Error, Try Again', 400);
    }
}
