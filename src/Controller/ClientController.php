<?php

namespace App\Controller;

use App\Entity\Client;
use App\Service\ClientService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ClientController extends AbstractController
{

    /**
     * @Route("/api/v1/client", name="Add Client", methods={"POST", "HEAD"}, schemes={"HTTP"}, host="127.0.0.1")
     */
    public function index()
    {
        $request = Request::createFromGlobals();

        $data = $request->request->all();

        $entityManager = $this->getDoctrine()->getManager();

        $clienteService = new ClientService($entityManager);
        $client = $clienteService->create($data);

        return $this->setResponse($client);
    }

    /**
     * @Route("/client/edit/{id}", name="Update Client", methods={"PUT", "HEAD"}, schemes={"HTTP"}, host="127.0.0.1")
     */
    public function update($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $client = $entityManager->getRepository(Client::class)->find($id);

        if (!$client) {
            throw $this->createNotFoundException('No client found for id' . $id);
        }

        $client->setName('Luiz');
        $client->setLastName('Zaparoli');

        $entityManager->flush();

        return new Response('Updated client with id' . $id);
    }

    /**
     * @Route("/client/{id}", name="Delete Client", methods={"DELETE", "HEAD"}, schemes={"HTTP"}, host="127.0.0.1")
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $client = $entityManager->getRepository(Client::class)->find($id);

        if (!$client) {
            throw $this->createNotFoundException('No client found for id' . $id);
        }

        $entityManager->remove($client);
        $entityManager->flush();

        return new Response('Deleted client with id' . $id);
    }


    /**
     * @Route("/api/v1/client/show", name="Show All Clients", methods={"GET", "HEAD"}, schemes={"HTTP"}, host="127.0.0.1")
     */
    public function show()
    {
        $client = $this->getDoctrine()->getRepository(Client::class);
        $data = $client->findAllClients();

        if (!$client) {
            throw $this->createNotFoundException('No client found');
        }

        return $this->render('base.html.twig', ['clients' => $data]);

        //return new JsonResponse($data, 200);
    }

    public function setResponse($content)
    {
        if ($content) {
            return new JsonResponse($content, 200);
        } else {
            return new Response(null ,400);
        }
    }
}