<?php

namespace App\Validation;

use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as v;

class ClientValidation
{

    public function formValidation($data)
    {

        $clientValidation =
                v::key('name', v::stringType()->notEmpty()->length(1,30))
                    ->key('last_name', v::stringType()->notEmpty()->length(1,30))
                    ->key('email', v::email()->notEmpty());

        try {
            $clientValidation->assert($data);
        } catch(NestedValidationException $exception) {
            print_r($exception->findMessages([
                'name'      => 'Campo {{name}} inválido.',
                'last_name' => 'Campo {{name}} inválido.',
                'email'     => 'Campo {{name}} inválido.',
            ]));
        }

        return $clientValidation->validate($data);
    }

}