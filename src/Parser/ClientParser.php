<?php

namespace App\Parser;

class ClientParser
{
    public function parserCliente($data)
    {
        $data = [
            'name'      => $data['name'],
            'last_name' => $data['last_name'],
            'email'     => $data['email'],
        ];

        return $data;
    }
}