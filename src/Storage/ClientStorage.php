<?php

namespace App\Storage;

use App\Entity\Client;
use Doctrine\ORM\EntityManager;

class ClientStorage
{
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Client $client)
    {
        $this->entityManager->persist($client);
        $this->entityManager->flush();

        return $client;
    }

    public function remove(Client $client)
    {
        $this->entityManager->remove($client);
        $this->entityManager->flush();

        return $client;
    }
}
