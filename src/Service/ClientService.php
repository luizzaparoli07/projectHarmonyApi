<?php

namespace App\Service;

use App\Entity\Client;
use App\Parser\ClientParser;
use App\Storage\ClientStorage;
use App\Validation\ClientValidation;
use Doctrine\ORM\EntityManager;

class ClientService
{
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create($data)
    {
        $client = null;

        $clientParser = new ClientParser();
        $parserClient = $clientParser->parserCliente($data);

        $clientValidation = new ClientValidation();
        $clientIsValid = $clientValidation->formValidation($parserClient);

        if ($clientIsValid) {
            $clientStorage = new ClientStorage($this->entityManager);

            $client = new Client();

            $client->setName($data['name']);
            $client->setLastName($data['last_name']);
            $client->setEmail($data['email']);
            $clientStorage->save($client);

            return $data;
        }
    }

}